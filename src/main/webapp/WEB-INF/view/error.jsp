<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>登陆页面</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="css/qlogin_v2.min.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
	    function getAuthorizations(){
	        var authorizations = "";
	        $("input:checkbox[name='api_choose']:checked'").each(function(i){  
	            if(0==i){  
	            	authorizations = $(this).attr('value');
	            }else{  
	            	authorizations += (","+$(this).attr('value'));
	            }  
	        }); 
	        
	        $("#authorizations").attr("value",authorizations);
// 	        alert($("#authorizations").val());
	    }
	</script>
<!-- @fragment_top End -->
</head>
<body>
<!-- @header Start-->
<div class="lay_top">
    <div class="lay_top_inner">
        <h1 class="logo text_hide">账号登录</h1>
        <div class="lat_top_other">
            <a href="" target="_blank" title="什么是QQ登录"><i class="icon_help_white"></i>账号登录</a>
            <span class="line">|</span>
            <a href="" id="" target="_blank" title="登录授权管理">授权管理</a>
            <span class="line">|</span>
            <a href="" target="_blank" title="申请接入">申请接入</a>
        </div>
    </div>
</div>
<!-- @header End -->
<div id="combine_page">
  <div class="page_login combine_page_children float_left border_right">
    <div class="lay_login_form">
    	<form action="authorize.do" method="POST" onsubmit="return getAuthorizations();">
			<table width="300" height="114">
				<tr>
					<td colspan="3">报错了！</td>
				</tr>
				<tr>
					<td colspan="3">原因：${error!''}</td>
				</tr>
				<tr>
					<td><a href="###" style="font-size:12px;">点此报错</a></td>
				</tr>
			</table>
	  </form>	
    </div>
  </div>
<!--   <div class="page_accredit combine_page_children float_left"> -->
<!--     <div class="lay_main" id="lay_main"> -->
<!--       <div class="lay_accredit_con"> -->
<!--         <p class="cnt_wording">该网站已有超过1万用户使用乐养账号登陆</p> -->
<!--         <p class="app_site_wording"><a class="accredit_site" id="accredit_site_link" href="" target="_blank">应用名</a>将获得以下权限：</p> -->
<!--         <div class="accredit_info" id="accredit_info"> -->
<!--           <ul class="accredit_info_op"> -->
<!--             <li class="select_all_li"> -->
<!--               <input type="checkbox" id="select_all" class="checkbox oauth_checkbox_all" hidefocus="true" checked="checked"> -->
<!--               <label class="oauth_item_title" for="select_all">全选</label> -->
<!--             </li> -->
<!--             <li> -->
<!--               <input name="api_choose" hidefocus="true" type="checkbox" class="checkbox oauth_checkbox" id="item_80901010" value="1" title="默认授权 不可更改" checked disabled /> -->
<!--               <label  for="item_80901010" class="oauth_item_title">测试资源1</label> -->
<!--             </li> -->
<!--             <li> -->
<!--               <input name="api_choose" hidefocus="true" type="checkbox" class="checkbox oauth_checkbox" id="item_2010" value="2" title="" checked disabled  /> -->
<!--               <label  for="item_2010" class="oauth_item_title">测试资源2</label> -->
<!--             </li>   -->
<!--             <li> -->
<!--               <input name="api_choose" hidefocus="true" type="checkbox" class="checkbox oauth_checkbox" id="item_2010" value="3" title="" checked disabled  /> -->
<!--               <label  for="item_2010" class="oauth_item_title">测试资源3</label> -->
<!--             </li>            -->
<!--           </ul> -->
<!--         </div> -->
<!--         <div class="oauth_tips_div"> -->
<!--           <p class="oauth_tips">授权后表明你已同意 <a href="" target="_blank">乐养登录服务协议</a></p> -->
<!--         </div> -->
<!--       </div> -->
<!--     </div> -->
<!--   </div> -->
</div>
<!--@fragment_bottom End -->
</body>
</html>
