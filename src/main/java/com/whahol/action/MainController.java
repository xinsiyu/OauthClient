package com.whahol.action;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.whahol.model.OAuthParams;

@Controller("MainController")
@RequestMapping("/")
public class MainController{

	
	@RequestMapping("login")
	public ModelAndView login(HttpServletRequest req){
		OAuthParams oauthParams = new OAuthParams();
		oauthParams.setClientId(req.getParameter("client_id"));
		oauthParams.setClientSecret(req.getParameter("client_secret"));
		oauthParams.setRedirectUri(req.getParameter("redirect_uri"));
		oauthParams.setState(req.getParameter("state"));
		oauthParams.setScope(req.getParameter("scope"));//非必要
		oauthParams.setRequestType(req.getParameter("request_type"));
		ModelMap map = new ModelMap();
		map.put("oauthParams", oauthParams);
		return new ModelAndView("login",map);
	}

}
