package com.whahol.action;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.whahol.enumType.ErrorType;
import com.whahol.model.OAuthParams;

@Controller("ErrorController")
@RequestMapping("/")
public class ErrorController{

	
	@RequestMapping("error")
	public ModelAndView login(@ModelAttribute("oauthParams") OAuthParams oauthParams,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap){
		Integer error = Integer.valueOf(request.getParameter("error"));
		ModelMap map = new ModelMap();
        map.put("error", ErrorType.getName(error));
		return new ModelAndView("error",map);
	}

}
