package com.whahol.util;

import java.util.Date;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

 
/**
 * json工具类
 * @author wzg
 *
 */
public class JsonUtil {  
	/**
	 * 使用flexjson把对象转换json对象<br>
	 * flexjson  转换耗时比较快
	 * @param excludeName 需要过虑掉的属性名<br>
	 * 			可变参数 用户可以如下四种方式使用:<br>
	 * 			JsonUtil.getJSONSerializer();<br>
	 * 			JsonUtil.getJSONSerializer(arg0);<br>
	 * 			JsonUtil.getJSONSerializer(arg0,arg1);<br>
	 * 			JsonUtil.getJSONSerializer(new Object[arg0,arg1,arg2]);
	 * @return 返回json对象
	 */
	public static JSONSerializer getJSONSerializer(Object... excludeName) {
		JSONSerializer jsonserializer = new JSONSerializer();
		jsonserializer.exclude(new String[] { "*.class" });
		if (excludeName != null) {
            for (int i = 0; i < excludeName.length; i++) {
                jsonserializer.exclude(excludeName[i].toString());
            }
        }
		jsonserializer.transform(new DateTransformer("yyyy-MM-dd HH:mm:ss"),
				new Class[] {Date.class});
		return jsonserializer;
	}
	
} 

