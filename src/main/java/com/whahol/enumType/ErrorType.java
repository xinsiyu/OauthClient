package com.whahol.enumType;

public enum ErrorType {
    UNKOWN_CLIENT_ID("未知的应用ID!", 101), 
    UNKOWN_CLIENT_SECRET("未知的应用SECRET!", 102),
    INVALID_AUTHORIZATION_CODE("错误的授权码", 103),
    INVALID_USERNAME_OR_PASSWORD("错误的用户名或密码", 104),
    CLIENT_ID_IS_NULL("应用ID不能为空!", 105),
    BAD_RQUEST("错误的请求!", 106);

    private String name;
    private int index;

    private ErrorType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(int index) {
        for (ErrorType c : ErrorType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    
    public static void main(String[] args) {
    	System.out.println(ErrorType.getName(106));
    }
}
